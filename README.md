Android Memory Acquisition <br>
https://github.com/504ensicsLabs/LiME <br>

Android Malware Threat Hunting <br>

https://files.brucon.org/2018/13-Christopher-Leroy-Hunting-Android-Malware.pdf <br>

https://securityintelligence.com/diy-android-malware-analysis-taking-apart-obad-part-1/ <br>

https://info.lookout.com/rs/051-ESQ-475/images/lookout-pegasus-android-technical-analysis.pdf <br>

https://citizenlab.ca/2023/04/spyware-vendor-quadream-exploits-victims-customers/ <br>

https://www.microsoft.com/en-us/security/blog/2023/04/11/dev-0196-quadreams-kingspawn-malware-used-to-target-civil-society-in-europe-north-america-the-middle-east-and-southeast-asia/ <br>

https://about.fb.com/wp-content/uploads/2022/12/Threat-Report-on-the-Surveillance-for-Hire-Industry.pdf <br>

https://github.com/brompwnie <br>

Go Lang <br>
https://github.com/gokatas <br>